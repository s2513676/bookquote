package nl.utwente.di.bookQuote;

import java.util.HashMap;
import java.util.Map;

public class Quoter {

    private Map<String, Double> inputs;

    public Quoter() {
        inputs = new HashMap<String, Double>();
        inputs.put("1", 10.0);
        inputs.put("2", 45.0);
        inputs.put("3", 20.0);
        inputs.put("4", 35.0);
        inputs.put("5", 50.0);
        inputs.put("others", 0.0);
    }

    double getBookPrice(double isbn) {

        return ((isbn * 9) / 5) + 32;

    }

}
